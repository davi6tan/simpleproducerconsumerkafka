package kafkaproject;

import java.util.Arrays;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class SimpleConsumer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String topicName = "topic1";
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "test");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
		consumer.subscribe(Arrays.asList(topicName));
		System.out.println("message is received from " + topicName);
		int i = 0;
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(100);
			for (ConsumerRecord<String, String> record : records) {
				System.out.println("Message: " + record);
			}
		}
	}

}
/*
 message is received from topic1
Message: ConsumerRecord(topic = topic1, partition = 0, offset = 8, key = 1, value = 1)
Message: ConsumerRecord(topic = topic1, partition = 0, offset = 9, key = 5, value = 5)
Message: ConsumerRecord(topic = topic1, partition = 0, offset = 10, key = 7, value = 7)
Message: ConsumerRecord(topic = topic1, partition = 0, offset = 11, key = 8, value = 8)
Message: ConsumerRecord(topic = topic1, partition = 1, offset = 4, key = 4, value = 4)
Message: ConsumerRecord(topic = topic1, partition = 1, offset = 5, key = 6, value = 6)
Message: ConsumerRecord(topic = topic1, partition = 2, offset = 8, key = 0, value = 0)
Message: ConsumerRecord(topic = topic1, partition = 2, offset = 9, key = 2, value = 2)
Message: ConsumerRecord(topic = topic1, partition = 2, offset = 10, key = 3, value = 3)
Message: ConsumerRecord(topic = topic1, partition = 2, offset = 11, key = 9, value = 9)
Message: ConsumerRecord(topic = topic1, partition = 0, offset = 12, key = 1, value = 1)
Message: ConsumerRecord(topic = topic1, partition = 0, offset = 13, key = 5, value = 5)
Message: ConsumerRecord(topic = topic1, partition = 0, offset = 14, key = 7, value = 7)
Message: ConsumerRecord(topic = topic1, partition = 0, offset = 15, key = 8, value = 8)
Message: ConsumerRecord(topic = topic1, partition = 2, offset = 12, key = 0, value = 0)
Message: ConsumerRecord(topic = topic1, partition = 2, offset = 13, key = 2, value = 2)
Message: ConsumerRecord(topic = topic1, partition = 2, offset = 14, key = 3, value = 3)
Message: ConsumerRecord(topic = topic1, partition = 2, offset = 15, key = 9, value = 9)
Message: ConsumerRecord(topic = topic1, partition = 1, offset = 6, key = 4, value = 4)
Message: ConsumerRecord(topic = topic1, partition = 1, offset = 7, key = 6, value = 6)
 
 */